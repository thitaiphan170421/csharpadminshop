﻿using _19T1021223.BusinessLayers;
using _19T1021223.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace _19T1021223.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        // GET: Account
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Trang đăng nhập vào hệ thống
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string userName = "", string password = "")
        {
            ViewBag.UserName = userName;
            if(string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password)) {
                ModelState.AddModelError("", "Thông tin không đầy đủ");
                return View();
            }
            var userAccount = UserAccountService.Authorize(AccountTypes.Employee, userName, password);
            if(userAccount == null)
            {
                ModelState.AddModelError("", "Đăng nhập thất bại");
                return View();
            }

            //Ghi cookie cho phiên đăng nhập
            string cookieString = Newtonsoft.Json.JsonConvert.SerializeObject(userAccount);
            FormsAuthentication.SetAuthCookie(cookieString, false);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ChangePassword()
        {
            AccountPagniationResultModel data = new AccountPagniationResultModel()
            {
                UserName = "",
                Password = "",
                NewPassword = "",
                PasswordAgain = ""
            };
            return View(data);
        }

        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ChangePassword(AccountPagniationResultModel data)
        {
            if (string.IsNullOrWhiteSpace(data.Password))
                ModelState.AddModelError("password", "Vui lòng nhập mật khẩu cũ");

           if (UserAccountService.Authorize(AccountTypes.Employee, Converter.CookieToUserAccount(User.Identity.Name).Email , data.Password ?? "") == null)
                ModelState.AddModelError("password", "Mật khẩu không đúng");

            if (string.IsNullOrWhiteSpace(data.NewPassword))
                ModelState.AddModelError("newPassword", "Vui lòng nhập mật khẩu mới");

            if (!string.IsNullOrWhiteSpace(data.NewPassword) && data.NewPassword.Length < 6 )
                ModelState.AddModelError("newPassword", "Mật khẩu phải lớn hơn 6 kí tự");

            if (string.IsNullOrWhiteSpace(data.PasswordAgain))
                ModelState.AddModelError("passwordAgain", "Vui lòng nhập lại mật khẩu mới");

           if (data.NewPassword != data.PasswordAgain && !string.IsNullOrEmpty(data.NewPassword) && !string.IsNullOrEmpty(data.PasswordAgain))
                ModelState.AddModelError("passwordAgain", "Mật khẩu không trùng khớp với mật khẩu mới");


            if (!ModelState.IsValid)
            {
                return View(data);
            }

            var changePasswordUser = UserAccountService.ChangePassword(AccountTypes.Employee, Converter.CookieToUserAccount(User.Identity.Name).Email, data.Password, data.NewPassword);

            if (!changePasswordUser)
            {
                ModelState.AddModelError("", "Đổi mật khẩu thất bại");
                return View();
            }

            Session.Clear();
            return RedirectToAction("Login");
        }
}
    }