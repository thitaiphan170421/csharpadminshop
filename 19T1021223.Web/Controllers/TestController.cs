﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _19T1021223.Web.Controllers
{
    [RoutePrefix("thu-nghiem")]
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        [Route("xin-chao/{name?}/{age?}")]
        public String SayHello(string name ="", int age = 18)
        {
            return $"Hello {name} , {age} years old";
        }
    }
}