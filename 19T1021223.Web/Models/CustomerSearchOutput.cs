﻿using _19T1021223.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021223.Web.Models
{
    /// <summary>
    /// Kết quả tìm kiếm , phân trang đối với khách hàng
    /// </summary>
    public class CustomerSearchOutput : PaginationSearchOutput
    {
        public List<Customer> Data { get; set; }
    }
}