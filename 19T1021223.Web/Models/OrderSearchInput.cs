﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021223.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderSearchInput: PaginationSearchInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
    }
}