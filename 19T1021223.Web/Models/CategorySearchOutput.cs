﻿using _19T1021223.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021223.Web.Models
{
    /// <summary>
    /// Kết quả tìm kiếm và phân trang của loại hàng
    /// </summary>
    public class CategorySearchOutput : PaginationSearchOutput
    {
        public List<Category> Data { get; set; }
    }
}