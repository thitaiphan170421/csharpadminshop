﻿using _19T1021223.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021223.Web.Models
{
    /// <summary>
    /// kết quả tìm kiếm và lấy dữ liệu mặt hàng dưới dạng phân trang
    /// </summary>
    public class ProductSearchOutput : PaginationSearchOutput
    {
        public List<Product> Data { get; set; }
    }
}