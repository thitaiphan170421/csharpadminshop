﻿using _19T1021223.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021223.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderSearchOutput: PaginationSearchOutput
    {
        public List<Order> Data { get; set; }
    }
}