﻿using _19T1021223.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021223.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderDetailModel
    {
        public Order Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<OrderDetail> OrderDetail { get; set; }

        public decimal TotalPriceOrder
        {
            get
            {
                decimal Sum = 0;
                foreach(var item in OrderDetail)
                {
                    Sum += item.TotalPrice;
                };
                return Sum;
            }
        }
    }
}