﻿using _19T1021223.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021223.DataLayers.SQLServer
{
    /// <summary>
    /// Cài đặt cho tài khoản của nhân viên
    /// </summary>
    public class EmployeeAccountDAL : _BaseDAL, IUserAccountDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public EmployeeAccountDAL(string connectionString) : base(connectionString)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public UserAccount Authorsize(string userName, string password)
        {
            UserAccount data = null;

            using(var connection = OpenConnection())
            {
                var cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM Employees WHERE Email = @Email AND Password = @Password";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Email", userName);
                cmd.Parameters.AddWithValue("@Password", password);

                using(var dbRender = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    if (dbRender.Read())
                    {
                        data = new UserAccount()
                        {
                            UserID = Convert.ToString(dbRender["EmployeeID"]),
                            UserName = Convert.ToString(dbRender["Email"]),
                            FullName = $"{dbRender["FirstName"]} {dbRender["LastName"]}",
                            Email = Convert.ToString(dbRender["Email"]),
                            Photo = Convert.ToString(dbRender["Photo"]),
                            Password = "",
                            RoleNames = ""
                        };
                    }

                    dbRender.Close();
                }

                connection.Close();
            }

            return data;
        }

        /// <summary>
        /// Đổi mật khẩu của khách hàng
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public bool ChangePassword(string userName, string password, string newPassword)
        {
            bool result = false;
            using(var connection = OpenConnection())
            {
                var cmd = connection.CreateCommand();
                cmd.CommandText = @"Update Employees set Password = @NewPassword where Email = @Email AND Password = @Password";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Email", userName);
                cmd.Parameters.AddWithValue("@Password", password);
                cmd.Parameters.AddWithValue("@NewPassword", newPassword);

                result = cmd.ExecuteNonQuery() > 0;

                connection.Close();
            }

            return result;
        }

        /*/// <summary>
        /// Kiểm tra xem tài khoản với mật khẩu có tồn tại hay không?
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>

        public bool InPassword (string userName , string password)
        {
            bool result = false;
            using(var connection = OpenConnection())
            {
                var cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT CASE WHEN EXISTS(SELECT * FROM Employees
                                    WHERE Email = @Email and Password = @Password) THEN 1 
                                    ELSE 0 END";

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Email", userName);
                cmd.Parameters.AddWithValue("@Password", password);

                result = Convert.ToBoolean(cmd.ExecuteScalar());

                connection.Close();
            }

            return result;
        }*/
    }
}
