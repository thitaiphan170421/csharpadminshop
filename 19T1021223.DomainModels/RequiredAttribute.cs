﻿using System;

namespace _19T1021223.DomainModels
{
    internal class RequiredAttribute : Attribute
    {
        public string ErrorMessage { get; set; }
    }
}