﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19T1021223.DomainModels
{
    /// <summary>
    /// Nhân viên giao hàng
    /// </summary>    
    public class Shipper
    {
        /// <summary>
        /// Mã nhân viên giao hàng
        /// </summary>
        public int ShipperID { get; set; }

        /// <summary>
        /// Tên nhân viên giao hàng
        /// </summary>
        public string ShipperName { get; set; }

        /// <summary>
        /// Số điện thoại nhân viên giao hàng
        /// </summary>
        public string Phone { get; set; }
    }
}
